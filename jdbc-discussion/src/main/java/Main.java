import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {

    public static void main(String[] args) throws Exception {
        String url = "jdbc:mysql://localhost:3306/jdbc";
        String username = "root";
        String password = "";
        //       String query = "SELECT * FROM artists WHERE id = 1";
        // String query = "SELECT * FROM artists;
        String query = "INSERT INTO artists (name) VALUES ('Joe')";

        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection myConnection = DriverManager.getConnection(url, username, password);

        //This interface is used to execute SQL statements
        Statement statement = myConnection.createStatement();

        //Provide methods to access the result row by row
//        ResultSet resultset = statement.executeQuery(query);

        int count = statement.executeUpdate(query);
        System.out.println(count + " row/s affected");
//
//        resultset.next();
//        String nextName = resultset.getString("name");
//        int nextId = resultset.getInt("id");
//
//        System.out.println(nextName);
//        System.out.println(nextId);
//
//
//        //Goes to the next row and returns it/ return false if no other records
//        while(resultset.next()) {
//            String name = resultset.getString("name");
//            int id = resultset.getInt("id");
//
//            System.out.println(name);
//            System.out.println(id);
//        }
//
//        //getString() add getInt() methods are used to retrieve values from columns with specific data types
//        String name = resultset.getString("name");
//        int id = resultset.getInt("id");
//
//        System.out.println(name);
//        System.out.println(id);
//
//        //The connection to the database and the statement input must be closed after using it
//        statement.close();
//        myConnection.close();

    }
}